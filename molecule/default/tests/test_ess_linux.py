import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-repository-ess-linux')


def test_repolist(host):
    cmd = host.run('dnf repolist')
    content = host.run('ls /etc/yum.repos.d')
    cat = host.run('cat /etc/yum.repos.d/oe-remote-repo-artifactory-rpm-ics-yocto-cct.repo')
    assert 'ESS ICS RPM repository: yocto cct cct_64' in cmd.stdout, \
        "Dir contents: {}\n{}".format(content.stdout, cat.stdout)
    assert 'oe-remote-repo-artifactory-rpm-ics-yocto-cct-cct_64' not in cmd.stdout, \
        "Dir contents: {}\n{}".format(content.stdout, cat.stdout)


def test_ics_install(host):
    cmd = host.run('dnf install -y gdb')
    assert cmd.rc == 0
