import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-repository-ubuntu')


def test_ethtool_ubuntu_install(host):
    cmd = host.run('apt install -y ethtool')
    assert cmd.rc == 0
